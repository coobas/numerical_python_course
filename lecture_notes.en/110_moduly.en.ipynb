{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python organizes everything into *modules*, which are used to separate *namespaces*. Modules can be organized into *packages* thus providing a tree structure of arbitrary depth. Python itself contains an extensive library of *built-in modules* (the *standard library*).\n",
    "<!-- TEASER_END -->"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Creating and using a module\n",
    "\n",
    "Module is basically created by saving a file. This module can then be executed directly (we are rather talking about a script) or, typically, the module is *imported*. This is done using the `import` keyword. Once the module is imported, you can use its symbols (functions, classes, global variables, etc.).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This snippet creates a file `thisisamodule.py` in the current directory. This will create a module `thisisamodule`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Writing thisisamodule.py\n"
     ]
    }
   ],
   "source": [
    "%%file thisisamodule.py\n",
    "\n",
    "# -*- coding: utf-8 -*-\n",
    "def foo():\n",
    "    print(u\"Doing nothing\")\n",
    "    return None\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can import this module ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import thisisamodule"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ".. and call its function `foo`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Doing nothing\n"
     ]
    }
   ],
   "source": [
    "thisisamodule.foo()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also import only some of the module's symbols using `from ... import ...`. Let's import `path` from the built-in module `sys`, which is the list of directories in which Python looks for modules.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['',\n",
       " '/sw/python2/anaconda3/envs/python_course/lib/python34.zip',\n",
       " '/sw/python2/anaconda3/envs/python_course/lib/python3.4',\n",
       " '/sw/python2/anaconda3/envs/python_course/lib/python3.4/plat-linux',\n",
       " '/sw/python2/anaconda3/envs/python_course/lib/python3.4/lib-dynload',\n",
       " '/sw/python2/anaconda3/envs/python_course/lib/python3.4/site-packages',\n",
       " '/sw/python2/anaconda3/envs/python_course/lib/python3.4/site-packages/Sphinx-1.3.1-py3.4.egg',\n",
       " '/sw/python2/anaconda3/envs/python_course/lib/python3.4/site-packages/setuptools-18.0.1-py3.4.egg',\n",
       " '/sw/python2/anaconda3/envs/python_course/lib/python3.4/site-packages/IPython/extensions']"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from sys import path\n",
    "path"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also rename modules or its symbols by `... import ... as ...`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n"
     ]
    }
   ],
   "source": [
    "# renames sys to system\n",
    "import sys as system\n",
    "# renames sys.path to syspath\n",
    "from sys import path as syspath\n",
    "# let's try if system.path is identical to syspath\n",
    "print(system.path is syspath)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also import everything from a module using `from ... import *`. This is only recommended in interactive use and only if you know what you're doing. Otherwise, we can easily rewrite other symbols.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running module as a script\n",
    "To use a module as a script (ie. by executing `python file_name.py`, alternatively `%run` in IPython), we must test the value of `__name__`. This variable contains the name of the module; however, once it's executed as a script, it contains \"\\_\\_main\\_\\_\". Thus a module that does something when executed will contain someting like\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello user!\n"
     ]
    }
   ],
   "source": [
    "def hello(how, whom):\n",
    "    print(\"{} {}!\".format(how, whom))\n",
    "    \n",
    "if __name__ == \"__main__\":\n",
    "    hello(\"Hello\", \"user\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Packages\n",
    "Single-level hierarchy of modules would surely not be soon enough so it is possible to organize modules into *packages*. A package is a directory with a file named `__init__.py`. This file can even be empty. We then place the package's modules into this directory. To import these modules we use `package_name.module_name`.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See more in [documentation](http://docs.python.org/2/tutorial/modules.html)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.4.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
